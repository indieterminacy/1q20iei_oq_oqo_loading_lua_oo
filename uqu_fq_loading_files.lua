#!/usr/bin/lua

class = require 'oo'

-- TODO Make functions table for Luajit
local ue = require("uw_outputting").te
local uw = require("uw_outputting").je

-- pathways
nqn = {}

-- data
tq = {}

-- loading
uqu = {}

-- classes
Mq = class()
Mq.name = 'Generic Content'

function uqu.fe (fe)
    require "os"
    local te
    local err
    local tw
    -- GIVEN type is table
    -- WHEN available pairs iterated as a file
    -- THEN each file read
    if type(fe) == "table" then
        local iei
        local nen_fe
        for iei, nen_fe in pairs(fe) do
            te,err = io.open( nen_fe, "r")
            if not err then
                tw = te:read('*a')
                te:close()
                return tw
            end
        end
    else
    -- GIVEN type is file
    -- WHEN file parameter
    -- THEN read file
        te,err = io.open(fe,"r")
        if not err then
            tw = te:read('*a')
            te:close()
            return tw
        end
    end
end

function Mq:abook_content()
    -- TODO Make local
    rex = require "rex_pcre"
    local nqn = self.home .. self.e .. self.w .. self.name
    local te = uqu.fe(nqn)
    ded = "^.*\n(%[0%].*)\n$"
    -- TODO Use updated gsub command
    return string.gsub(te, ded, "%1")
end

nqn.abook = Mq:extend {
    home = os.getenv ("HOME") .. "/",
    -- TODO Modernise pathways
    e = "2q_doc-qiuy/20_Content/",
    w = "rqr_record-fairs/kq_abook/"
}

-- TODO Expand
-- TODO Consider metatables
tq.abook = Mq:extend {
    __init = function(self)
        self.name = self.name
    end
}

function uqu.abook (fe)
    Rqr_tq_event_data = nqn.abook:extend(tq.abook)
    c = Rqr_tq_event_data{ name=fe }
    return c:abook_content()
end

-- ue(uqu.abook("rqr_record-fairs"))
return uqu
